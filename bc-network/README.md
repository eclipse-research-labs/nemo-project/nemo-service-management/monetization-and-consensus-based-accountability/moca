<!---
 Copyright 2024  MAGGIOLI SPA
 
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License.  You may obtain a copy
 of the License at
 
   http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 License for the specific language governing permissions and limitations under
 the License.
 
 SPDX-License-Identifier: Apache-2.0
-->

# NEMO Blockchain Network

## Description

The present project is a Helm chart responsible for deploying the nodes of a Quorum network and the Smart Contracts we want to access.

## Installation

To install the Helm chart, run the following:
```bash
helm install quorum charts/quorum -f charts/quorum/values.yaml 
```

To upgrade the chart, run the following:
```bash
helm upgrade --reuse-values quorum -f charts/quorum/values.yaml charts/quorum
```

To delete the chart, run the following:
```bash
helm delete quorum  
```
