<!---
 Copyright 2024  MAGGIOLI SPA
 
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License.  You may obtain a copy
 of the License at
 
   http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 License for the specific language governing permissions and limitations under
 the License.
 
 SPDX-License-Identifier: Apache-2.0
-->

# NEMO MOCA

## Description

MOCA supports the pre-commercial exploitation of the NEMO platform across the multi-operator/multi-tenant IoT/5G continuum. This mechanism implements a consensus-based distributed architecture for sharing networking, computing, and storage resources from various end-users and (competing) telecom and cloud providers. This approach enables the creation of new business models allowing volunteers and professionals to adopt the NEMO platform and offer hosting and migration services according to the resources as a service (RaaS) paradigm. MOCA offers a traceable way to build future business trade-offs between providers sharing bundles of computing, memory, storage resources and I/O resources for a short period of time based on DLT-based smart contracts. To achieve its goals, MOCA collaborates with the meta-Orchestrator, the CMDT and the monitoring framework. 
The MOCA component provides a secure way to engage end-users and infrastructure providers to adopt NEMO platform for deploying their services or/and offering resources. Despite the fact the MOCA will not support financial transactions it proposes a monetization approach that can be used in the future for financial transactions between the stakeholders. The main idea is based on the implementation of the RaaS model, through which the shareholders can “sell” individual resources for a few seconds at a time so that end users can benefit from a wide and dynamic continuum of heterogeneous types of resources. Following this approach, the providers can exploit their idle resources and the end users can utilize these resources in order to achieve their services SLOs at best prices. The system will trace all transactions using DLT technology and Smart Contracts and will provide accounting services based on the utilization of the resources.

![]() <img src="https://gitlab.eclipse.org/eclipse-research-labs/nemo-project/nemo-service-management/monetization-and-consensus-based-accountability/moca/-/raw/main/event-server/img/moca.png"  width="50%">

## Dependencies
1.	**Kubernetes**
2.  **Helm**
3.	**Docker**
4.  **Docker Compose**


## Installation

1. Initiation of the private Blockchain network
```bash
helm install quorum bc-network/charts/quorum -f bc-network/charts/quorum/values.yaml 
```
2. Initiations of the Smart Contract server

```bash
kubectl apply -f event-server/config/k8s
```

## Documantation

The MOCA API is documented in the following link: `http://<api-ip>:<api-port>/swagger` 


## Licence

Please see the LICENSE file for more details.
