#!/bin/sh

pipenv run python3 manage.py migrate --run-syncdb
# pipenv run python3 manage.py register_events
pipenv run gunicorn -b 0.0.0.0:8000 --threads 4 moca.wsgi 