import json
import logging
import os
import pika
import yaml

from django.conf import settings
from django.core.files import File 

from app.serializers import ClusterResourcesSerializer
from app.models import ClusterResources, Cluster, Handler

from app.utils.utils import Ipfs

log = logging.getLogger(__name__)
logging.getLogger('pika').setLevel(logging.WARNING)


class RabbitMQClient:
    def __init__(self):
        self.host = settings.NEMO_RABBITMQ_HOST,
        self.port = settings.NEMO_RABBITMQ_PORT
        self.credentials = pika.PlainCredentials(settings.NEMO_RABBITMQ_USER, settings.NEMO_RABBITMQ_PASSWORD)

    def make_connection(self):
        """ Returns a new pika block connection
        """
        return pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.NEMO_RABBITMQ_HOST, 
            port=settings.NEMO_RABBITMQ_PORT,
            credentials=self.credentials)
        )
    
    def subcribe_to_intent_api(self):
        with self.make_connection() as connection:
            channel = connection.channel()

            result = channel.queue_declare(queue=settings.NEMO_INTENT_API_MOCA_QUEUE, durable=True)
            queue_name = result.method.queue
            channel.queue_bind(exchange=settings.NEMO_RABBITMQ_EXCHANGE, queue=queue_name, routing_key=settings.NEMO_INTENT_API_MOCA_ROUTING_KEY)
            
            def callback(ch, method, properties, body):
                serializer_class = ClusterResourcesSerializer
                payload = json.loads(body)
                log.info(" [x] Received %r" % payload)

                serializer = serializer_class(data=payload)
                try:
                    if serializer.is_valid():
                        vd = serializer.validated_data

                        cr = ClusterResources.objects.create(**vd)
                        Cluster.objects.create(cluster_resources= cr, status='pending')

                        msg = {"id": str(cr.id), "name": cr.cluster_name, "memory": cr.memory, "cpus": cr.cpus, "storage": cr.storage}
                        self.publish_to_meta_os(msg=msg)
                except Exception as e:
                    log.error('Exception while parsing payload {}. Reason {}'.format(
                        payload, str(e)
                    ), exc_info=True)

                    cr.delete()

            channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

            log.info(' [*] Waiting for messages. To exit press CTRL+C')
            channel.start_consuming()
            
    def publish_to_meta_os(self, msg):
        with self.make_connection() as connection:
            channel = connection.channel()

            channel.exchange_declare(exchange=settings.NEMO_RABBITMQ_EXCHANGE, exchange_type='topic', durable=True)

            channel.basic_publish(exchange=settings.NEMO_RABBITMQ_EXCHANGE,
                        routing_key=settings.NEMO_RABBITMQ_ROUTING_KEY,
                        body=json.dumps(msg))

            log.info(f" [x] Sent to Meta Orchestrator!")

            connection.close()

    def subcribe_to_meta_os(self):
        with self.make_connection() as connection:
            channel = connection.channel()

            result = channel.queue_declare(queue=settings.NEMO_META_OS_MOCA_QUEUE, durable=True)
            queue_name = result.method.queue
            channel.queue_bind(exchange=settings.NEMO_RABBITMQ_EXCHANGE, queue=queue_name, routing_key=settings.NEMO_META_OS_MOCA_ROUTING_KEY)
            
            def callback(ch, method, properties, body):
                payload = json.loads(body)
                log.info(" [x] Received %r" % payload)

                id = payload['id']
                status = payload['status']

                try:
                    cr = ClusterResources.objects.get(id=id)
                    if 'config' in payload:
                        config = payload['config']


                        config_subname = id.split('-')[0]
                        config_name = f'config-{config_subname}'

                        if 'endpoint' in payload:
                            cr.endpoint = payload['endpoint']
                        else:
                            try:
                                y = yaml.safe_load(config)
                                cr.endpoint = y['clusters'][0]['cluster']['server']
                            except yaml.YAMLError as exc:
                                log.info(exc)


                        with open(config_name, "w+") as f:
                            tmp = File(f)
                            tmp.write(config)

                            cr.config_file = tmp


                            try:
                                ipfs = Ipfs()
                                error_message, cid, ipfs_file_link = ipfs.store_config_file(cr.config_file, id)

                                log.info(f'This is the CID: {cid}')

                                if error_message != "":
                                    raise Exception(error_message)

                                if cid != "" and ipfs_file_link != "":
                                    cr.save()
                                    Cluster.objects.filter(cluster_resources_id=id).update(status=status)
                                    Handler.objects.filter(cluster_id=id).update(link_cid=cid, ipfs_link=ipfs_file_link)

                                    log.info('The status and config file were updated')
                            except Exception as e:
                                log.error('Exception while writing to IPFS or Quorum {}. Reason {}'.format(
                                    payload, str(e)
                                ), exc_info=True)
                except Exception as e:
                    log.error('Exception while parsing payload {}. Reason {}'.format(
                        payload, str(e)
                    ), exc_info=True)
            
            channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

            log.info(' [*] Waiting for messages. To exit press CTRL+C')
            channel.start_consuming()