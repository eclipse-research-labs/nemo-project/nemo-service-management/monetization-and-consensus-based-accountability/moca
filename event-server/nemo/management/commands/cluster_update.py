import logging
import os
import sys

from django.core.management import BaseCommand

from nemo.rabbitmq import RabbitMQClient

log = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Parses requests from the Intent API'

    def handle(self, *args, **options):
        try:
            client = RabbitMQClient()

            client.subcribe_to_meta_os()

        except KeyboardInterrupt:
            print('Interrupted')
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)
