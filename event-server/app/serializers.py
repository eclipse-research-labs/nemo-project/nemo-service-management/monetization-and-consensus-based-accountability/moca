from rest_framework import serializers
from app.models import ClusterResources, Handler, Cluster

from eth_utils import is_hex_address, is_0x_prefixed, is_hex


class TransactionHashSerializer(serializers.Serializer):
    """The transaction hash.
    """
    tx_hash = serializers.CharField(max_length=66, min_length=66)

    def validate_tx_hash(self, tx_hash):
        if not is_hex(tx_hash) or not is_0x_prefixed(tx_hash):
            raise serializers.ValidationError('Transaction hash is not a valid 0x prefixed hex string')
        return tx_hash


class IpfsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Handler
        fields = '__all__'


class ClusterResourcesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClusterResources
        fields = '__all__'


class ClusterSerializer(ClusterResourcesSerializer):
    class Meta:
        model = Cluster
        fields = '__all__'


class UpdateClusterResourcesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClusterResources
        exclude = ('cluster_name',)
        read_only_fields = ('id',)


class UpdateClusterStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cluster
        fields = ('status',)
        read_only_fields = ('id',)
