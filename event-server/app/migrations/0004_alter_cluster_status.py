# Generated by Django 3.2.19 on 2024-02-21 09:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20240220_1606'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cluster',
            name='status',
            field=models.CharField(choices=[('pending', 'pending'), ('received_model', 'received_model'), ('deploy_in_progress', 'deploy_in_progress'), ('deploy_success', 'deploy_success'), ('deploy_failed', 'deploy_failed')], default='pending', help_text='The status of the deployment of the Cluster', max_length=42),
        ),
    ]
