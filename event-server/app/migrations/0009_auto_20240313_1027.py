# Generated by Django 3.2.19 on 2024-03-13 10:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_auto_20240313_0919'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cluster',
            old_name='cluster_resources_id',
            new_name='cluster_resources',
        ),
        migrations.RenameField(
            model_name='handler',
            old_name='cluster_resources_id',
            new_name='cluster_resources',
        ),
    ]
