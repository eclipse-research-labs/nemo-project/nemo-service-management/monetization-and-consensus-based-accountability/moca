import uuid
from django.db import models


class ClusterResources(models.Model):
    id = models.CharField(max_length=42, primary_key=True, unique=True, editable=False, default=uuid.uuid4,
                          help_text='The id of the Cluster')
    cluster_name = models.CharField(max_length=42, help_text='The name of the Cluster that will be deployed')
    config_file = models.FileField(blank=True, null=True, upload_to='uploads/',
                                   help_text='The Kubernetes configuration file that will be stored')
    cpus = models.IntegerField(blank=True, null=True, help_text='The number of CPUs of the Cluster')
    memory = models.IntegerField(blank=True, null=True, help_text='The RAM of the Cluster in GB')
    storage = models.IntegerField(blank=True, null=True, help_text='The disk storage of the Cluster in GB')
    endpoint = models.URLField(blank=True, null=True, help_text='The endpoint of the Kubernetes Cluster')
    timestamp = models.DateTimeField(auto_now_add=True, blank=True, null=True)


class Cluster(models.Model):
    STATES = (
        ('pending', 'pending'),
        ('received_model', 'received_model'),
        ('deploy_in_progress', 'deploy_in_progress'),
        ('deploy_success', 'deploy_success'),
        ('deploy_failed', 'deploy_failed')
    )

    cluster_resources = models.OneToOneField(ClusterResources, related_name='cluster', editable=False, default=uuid.uuid4,
                                             on_delete=models.CASCADE, primary_key=True, help_text='The id of the ClusterResources')
    status = models.CharField(max_length=42, choices=STATES, default='pending',
                              help_text='The status of the deployment of the Cluster')
    timestamp = models.DateTimeField(auto_now_add=True, blank=True, null=True)


class Handler(models.Model):
    cluster = models.OneToOneField(Cluster, related_name='handler', editable=False, default=uuid.uuid4,
                                             on_delete=models.CASCADE, primary_key=True, help_text='The id of the ClusterResources')
    link_cid = models.CharField(max_length=46, default=' ', editable=True,
                                help_text='The CID of the Cluster config stored in IPFS')
    ipfs_link = models.CharField(max_length=255, default=' ', editable=True,
                                 help_text='The link to retrieve the Cluster config')
    timestamp = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    

class Application(models.Model):
    STATES = (
        ('pending', 'pending'),
        ('running', 'running'),
        ('evicted', 'evocted'),
        ('failed', 'failed'),
        ('unknown', 'unknown')
    )

    id = models.CharField(max_length=42, primary_key=True, unique=True, editable=False, default=uuid.uuid4,
                          help_text='The id of the Application')
    app_name = models.CharField(max_length=42, help_text='The name of the Application that will be deployed')
    cpus = models.IntegerField(blank=True, null=True, help_text='The number of CPUs of the Application')
    memory = models.IntegerField(blank=True, null=True, help_text='The RAM of the Application in MB')
    storage = models.IntegerField(blank=True, null=True, help_text='The space of the volume in GB')
    endpoint = models.URLField(blank=True, null=True, help_text='The endpoint of the Application')
    status = models.CharField(max_length=42, choices=STATES, default='pending',
                              help_text='The status of the deployment of the Application')
    timestamp = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    
    

class Data(models.Model):
    id = models.CharField(max_length=42, primary_key=True, unique=True, editable=False, default=uuid.uuid4,
                          help_text='The id of the data')
    type = models.CharField(max_length=46, default='Generic dataset', editable=True,
                                help_text='The type of the dataset')
    description = models.CharField(max_length=80, default='Generic description', editable=True,
                                help_text='A description for the dataset')
    metadata = models.CharField(max_length=255, default=' ', editable=True,
                                 help_text='The metadata of the dataset')
    endpoint = models.URLField(blank=True, null=True, help_text='The endpoint of the S3 bucket where the dataset is accessible')
    timestamp = models.DateTimeField(auto_now_add=True, blank=True, null=True)
