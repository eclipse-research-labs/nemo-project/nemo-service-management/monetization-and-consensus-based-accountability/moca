import logging

from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from rest_framework import status
from rest_framework.generics import ListAPIView, UpdateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from app.serializers import ClusterResourcesSerializer, TransactionHashSerializer, IpfsSerializer, \
    UpdateClusterResourcesSerializer, UpdateClusterStatusSerializer
from app.models import ClusterResources, Handler, Cluster
from app.tasks import insert_link, get_link, write_config_file_to_ipfs

from app.utils.utils import Ipfs

import json

log = logging.getLogger(__name__)

BASE_RESPONSES = {
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Invalid credentials',
    status.HTTP_403_FORBIDDEN: 'Invalid permissions'
}

SC_RESPONSES = {
    status.HTTP_406_NOT_ACCEPTABLE: 'The smart contract rolled back (declined) the transaction',
}


class ClusterRegister(APIView):
    serializer_class = ClusterResourcesSerializer

    @swagger_auto_schema(request_body=serializer_class, responses={
        **BASE_RESPONSES,
        **SC_RESPONSES,
        status.HTTP_201_CREATED: openapi.Response('The generated transaction hash', TransactionHashSerializer),
    })
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        ipfs = Ipfs()
        if serializer.is_valid():
            vd = serializer.validated_data

            cr = ClusterResources.objects.create(**vd)
            
            c = Cluster.objects.create(cluster_resources= cr, status='pending')

            log.info(vd)

            if vd['config_file'] != None:

                try:
                    cluster_id = cr.id
                    config_file = vd['config_file']

                    error_message, cid, ipfs_file_link = ipfs.store_config_file(config_file, cluster_id)

                    if error_message != "":
                        raise Exception(error_message)


                    Handler.objects.create(cluster=c, link_cid=cid, ipfs_link=ipfs_file_link)

                except Exception:
                    msg = {"error": "The cluster config already exists!"}
                    cr.delete()

                    return Response(msg, status=status.HTTP_406_NOT_ACCEPTABLE)
                
            else:
                Handler.objects.create(cluster=c, link_cid='pending', ipfs_link='pending')
            
            return Response(cr.id, status=status.HTTP_201_CREATED)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ClusterRetrieve(ListAPIView):
    serializer_class = ClusterResourcesSerializer

    @swagger_auto_schema(responses={
        **BASE_RESPONSES,
        **SC_RESPONSES,
        status.HTTP_201_CREATED: openapi.Response('The details of all records', serializer_class),
    })
    def get(self, request, *args, **kwargs):
        all = Handler.objects.all()

        data = []
        for obj in all:
            merged_data = {
                'id': obj.cluster.cluster_resources.id,
                'vm_name': obj.cluster.cluster_resources.cluster_name,
                'cpus': obj.cluster.cluster_resources.cpus,
                'memory': obj.cluster.cluster_resources.memory,
                'storage': obj.cluster.cluster_resources.storage,
                'endpoint': obj.cluster.cluster_resources.endpoint,
                'status': obj.cluster.status,
                'ipfs': {
                    'link_id': obj.link_cid,
                    'ipfs_link': obj.ipfs_link
                }
            }
            data.append(merged_data)

        return JsonResponse(data, safe=False)


class ClusterRetrieveId(ListAPIView):
    serializer_class = ClusterResourcesSerializer

    @swagger_auto_schema(responses={
        **BASE_RESPONSES,
        **SC_RESPONSES,
        status.HTTP_201_CREATED: openapi.Response('The details of all records', serializer_class),
    })
    def get(self, request, *args, **kwargs):
        id = kwargs.get('id')

        result = get_object_or_404(Handler, cluster_id=id)

        data = []

        merged_data = {
            'id': result.cluster.cluster_resources.id,
            'vm_name': result.cluster.cluster_resources.cluster_name,
            'cpus': result.cluster.cluster_resources.cpus,
            'memory': result.cluster.cluster_resources.memory,
            'storage': result.cluster.cluster_resources.storage,
            'endpoint': result.cluster.cluster_resources.endpoint,
            'status': result.cluster.status,
            'ipfs': {
                'link_id': result.link_cid,
                'ipfs_link': result.ipfs_link
            }
        }
        data.append(merged_data)

        return JsonResponse(data, safe=False)


class UpdateClusterResources(UpdateAPIView):
    serializer_class = UpdateClusterResourcesSerializer

    @swagger_auto_schema(responses={
        **BASE_RESPONSES,
        **SC_RESPONSES,
        status.HTTP_200_OK: openapi.Response('The cluster details have been updated', serializer_class),
    })
    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        id = kwargs.get('id')

        if serializer.is_valid():
            vd = serializer.validated_data
            if any(vd) == False:
                error = {'Incorrect cluster specs!'}
                return Response(error, status=status.HTTP_400_BAD_REQUEST)
            else:
                get_object_or_404(ClusterResources, id=id)

                if 'config_file' in vd and (vd['config_file'] != None or vd ['config_file'] != ''):
                    try:
                        ipfs = Ipfs()
                        error_message, cid, ipfs_file_link = ipfs.store_config_file(vd['config_file'], id)

                        if error_message != "":
                            raise Exception(error_message)

                        if cid != "" and ipfs_file_link != "":
                            ClusterResources.objects.filter(id=id).update(**vd)
                            Handler.objects.filter(cluster_id=id).update(link_cid=cid, ipfs_link=ipfs_file_link)
                    except Exception:
                        msg = {"error": "The cluster config already exists!"}

                        return Response(msg, status=status.HTTP_406_NOT_ACCEPTABLE)           
                        
                else:
                    ClusterResources.objects.filter(id=id).update(**vd)

                msg = {f'Cluster with id {id} was updated successfully!'}
                return Response(msg, status=status.HTTP_200_OK)
            
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

class UpdateClusterStatus(UpdateAPIView):
    serializer_class = UpdateClusterStatusSerializer

    @swagger_auto_schema(responses={
        **BASE_RESPONSES,
        **SC_RESPONSES,
        status.HTTP_200_OK: openapi.Response('The cluster status has been updated', serializer_class),
    })
    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        id = kwargs.get('id')

        if serializer.is_valid():
            vd = serializer.validated_data
            if any(vd) == False:
                error = {'Incorrect cluster status!'}
                return Response(error, status=status.HTTP_400_BAD_REQUEST)
            else:
                get_object_or_404(Cluster, cluster_resources_id=id)
                Cluster.objects.filter(cluster_resources_id=id).update(**vd)

                msg = {f'Status of the cluster with id {id} was updated successfully!'}
                return Response(msg, status=status.HTTP_200_OK)
            
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    