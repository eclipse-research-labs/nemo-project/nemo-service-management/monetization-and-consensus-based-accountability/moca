from django_ethereum_events.chainevents import AbstractEventReceiver
from app.models import Handler
from app.utils.utils import ChainEvent
import logging

log = logging.getLogger(__name__)


class NewEventReceiver(AbstractEventReceiver):
    def save(self, decoded_event):
        log.info(decoded_event)
        chain_event = ChainEvent()
        decoded_event_dict = dict(decoded_event)
        args = dict(decoded_event_dict.pop('args'))

        event = chain_event.create_chain_event(args, decoded_event_dict)

        log.info('Event: {}'.format(event))

        Handler.objects.update_or_create(
            id=args['id'],
            defaults={
                'link_id': args['link_id'],
                'ipfs_link': args['ipfs_link']
            }
        )
