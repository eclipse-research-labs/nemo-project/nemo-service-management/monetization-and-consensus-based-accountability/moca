from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('moca/admin/', admin.site.urls),
    path('moca/', include(('app.urls', 'app'))),
]