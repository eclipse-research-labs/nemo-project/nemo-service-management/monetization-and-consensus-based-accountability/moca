from .base import *

DEBUG = False

ALLOWED_HOSTS = [
    'localhost',
    '127.0.0.1',
    '192.168.1.236'
]

# ==================================
#   POSTGRES SETTINGS
# ==================================
user = os.getenv('POSTGRES_USER')
password = os.getenv('POSTGRES_PASSWORD')
db_name = os.getenv('POSTGRES_DB')
db_port = os.getenv('POSTGRES_PORT')
db_host = os.getenv('POSTGRES_HOST')
      
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': db_name,
        'USER': user,
        'PASSWORD': password,
        'HOST': db_host,
        'PORT': 5432
    }
}

#   CELERY BEAT SETTINGS
# =================================
CELERY_TIMEZONE = 'UTC'
CELERYBEAT_SCHEDULE = {
    'ethereum_events': {
        'task': 'django_ethereum_events.tasks.event_listener',
        'schedule': timedelta(seconds=20)
    }
}

# ==================================
#   QUORUM SETTINGS
# ==================================
QUORUM_URL = os.getenv("QUORUM_URL")
QUORUM_CONTRACT_HOST = os.getenv('QUORUM_CONTRACT_HOST', default='node1')
# PUBLIC_KEY = os.getenv("PUBLIC_KEY")

# ==================================
#   IPFS SETTINGS
# ==================================
IPFS_HOST = os.getenv("IPFS_HOST")
IPFS_PORT = os.getenv("IPFS_PORT")
IPFS_BASE_LINK = os.getenv("IPFS_BASE_LINK")

# =================================
#   ETHEREUM-EVENTS SETTINGS
# =================================
ETHEREUM_POA = bool(strtobool(os.getenv('ETHEREUM_POA', default='True')))
ETHEREUM_NODE_URI = QUORUM_URL
ETHEREUM_GETH_POA = ETHEREUM_POA
ETHEREUM_NODE_SSL = False

# ==================================
#   NEMO RABBITMQ SETTINGS
# ==================================
NEMO_RABBITMQ_USER = os.getenv('NEMO_RABBITMQ_USER', default='')
NEMO_RABBITMQ_PASSWORD = os.getenv('NEMO_RABBITMQ_PASSWORD', default='')
NEMO_RABBITMQ_HOST = os.getenv('NEMO_RABBITMQ_HOST', default='')
NEMO_RABBITMQ_PORT = os.getenv('NEMO_RABBITMQ_PORT', default=5672)
NEMO_RABBITMQ_EXCHANGE = os.getenv('NEMO_RABBITMQ_EXCHANGE', default='')
NEMO_RABBITMQ_QUEUE = os.getenv('NEMO_RABBITMQ_QUEUE', default='')
NEMO_RABBITMQ_ROUTING_KEY = os.getenv('NEMO_RABBITMQ_ROUTING_KEY', default='')
NEMO_INTENT_API_MOCA_QUEUE = os.getenv('NEMO_INTENT_API_MOCA_QUEUE', default='')
NEMO_INTENT_API_MOCA_ROUTING_KEY = os.getenv('NEMO_INTENT_API_MOCA_ROUTING_KEY', default='')
NEMO_META_OS_MOCA_QUEUE=os.getenv('NEMO_META_OS_MOCA_QUEUE', default='')
NEMO_META_OS_MOCA_QUEUE=os.getenv('NEMO_META_OS_MOCA_QUEUE', default='')
NEMO_META_OS_MOCA_ROUTING_KEY=os.getenv('NEMO_META_OS_MOCA_ROUTING_KEY', default='')