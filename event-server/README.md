# NEMO Cluster Registration API

## Purpose

This is the Cluster Registration API, part of the [NEMO](https://meta-os.eu/) project. This API is used to register the information of a cluster  that is to join the NEMO infrastructure. The API stores the details of the cluster (e.g. name, memory, CPUs etc.) and uses the [Quorum](https://docs.goquorum.consensys.io/) blockchain network and [IPFS](https://docs.ipfs.tech/) technology to store and retrieve its config file.


## Technologies

1.	**Quorum version**: 22.7.1
2.	**Tessera version**: 1.0.0
3.	**IPFS version**: v0.15.0
4.	**Django Version**: 3.2.19 (~=3.2.2,<3.3)
5.	**Gunicorn version**: 20.1.0 (~=20.0.1)
6.	**Celery version**: 5.2.7 (~=5.2.4)

**Python version**: 3.9


## API endpoints

| Endpoint | HTTP Request | Explanation |
|   ---    |     ---      |     ---     |
| `/register` | `POST` | Register the information of the cluster |
| `/retrieve` | `GET` | Retrieve the information of the cluster stored |
| `/retrieve/<cluster-id>`  | `GET` | Retrieve the information of a particular cluster stored |

You can also visit the API Swagger: `http://<api-ip>:<api-port>/swagger` 

## Configure API
Before starting the API, there are a few environmental variables that need to be set. Create a `.env` file and add the variables, presented in the following table:

| Name | Description | Value |
|  --- |     ---     |  ---  |
| `POSTGRES_HOST` | The name of the postgres | `postgres` |
| `POSTGRES_DB` | The name of the postgres database | `postgres_db` |
| `POSTGRES_USER` | The postgres username | `user` |
| `POSTGRES_PASSWORD` | The postgres password | `password` |
| `CELERY_BROKER_URL` | The URL for the Celery broker (Redis Broker) | `redis://redis:6379` |
| `QUORUM_URL` | The URL of a Quorum node | `http://localhost` |
| `QUORUM_CONTRACT_HOST` | Name of the Quorum node | `node1` |
| `PUBLIC_KEY` | The public key of the Quorum node | `gohugaoITUTFDGKdsfi+gfasy` |
| `IPFS_HOST` | The IPFS host | `ipfs` |
| `IPFS_PORT` | The IPFS port | `5001` | 
| `IPFS_BASE_LINK` | The base link to retrieve a file stored in the IPFS node | `http://127.0.0.1:8080/ipfs` |

In case you are using the **Kubernetes** deployment method, make sure to change the environmental variables and Postgres credentials in: `config/k8s/nemo-api.yml` and `config/k8s/postgres.yml`.

## Deployment

* **Locally**:

```bash
cd app/app

pipev install

pipenv run django-admin runserver
```

* **Docker Compose**:

```bash
cd app/app

docker-compose up --build
```    

* **Kubernetes**:

```bash
kubectl apply -f config/k8s
```